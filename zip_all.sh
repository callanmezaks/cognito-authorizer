#!/bin/sh

rm -rf dependencies/
mkdir dependencies

pip install -r requirements.txt --target ./dependencies

#Zip all dependencies
cd dependencies
zip -r ../deploy.zip .

cd ..
zip -g deploy.zip authenticate.py
rm -rf dependencies/
